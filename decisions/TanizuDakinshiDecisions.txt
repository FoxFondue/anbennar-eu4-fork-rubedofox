
country_decisions = {

	end_the_warlord_era = {
		major = yes
		potential = {
			NOT = { has_global_flag = dakinshiland_unified }
			primary_culture = dakinshi
			normal_or_historical_nations = yes
			was_never_end_game_tag_trigger = yes
		}
		provinces_to_highlight = {
			province_group = dakinshiland
		}
		allow = {
			NOT = { has_global_flag = dakinshiland_unified }
			is_at_war = no
			is_nomad = no
			is_free_or_tributary_trigger = yes
			dakinshiland = {
				country_or_non_sovereign_subject_holds = ROOT
				type = all
			}
			#guifa_zahab_region = {
			#	is_city = yes
			#	country_or_non_sovereign_subject_holds = ROOT
			#	type = all
			#}
		}
		effect = {
			#change_government = theocracy
			add_government_reform = namasonamingo_reform
			swap_non_generic_missions = yes
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 2 }
				}
				set_government_rank = 2
			}
			every_subject_country = {
				limit = {
					primary_culture = dakinshi
				}
				every_owned_province = {
					add_core = ROOT
					cede_province = ROOT
				}
			}
			set_global_flag = dakinshiland_unified
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			dakinshiland = {
				add_base_tax = 1 #placeholder mostly
			}
			add_prestige = 25
			override_country_name = "Bwa Dakinshi"
		}
		ai_will_do = {
			factor = 400
		}
	}

	check_beast_memory = {
		major = yes
		potential = {
			religion = beast_memory
		}
		allow = {
			1794 = {
				check_variable = {
					which = BeastMemoryYearsLeft
					value = 0
				}
			}
			has_global_flag = beast_memory_mongoose
			has_global_flag = beast_memory_leopard
			has_global_flag = beast_memory_elephant
		}
		effect = {
		}
		ai_will_do = {
			factor = 400
		}
	}

	#testing 
	become_mongoose = {
		major = yes
		potential = {
			ai = no
			culture_group = tanizu
		}
		allow = {
			NOT = {
        	    personality = elephant_shifter_personality
        	    personality = mongoose_shifter_personality
        	    personality = leopard_shifter_personality
        	}
		}
		effect = {
        	add_ruler_personality = mongoose_shifter_personality
		}
		ai_will_do = {
			factor = 400
		}
	}
	become_leopard = {
		major = yes
		potential = {
			ai = no
			culture_group = tanizu
		}
		allow = {
			NOT = {
        	    personality = elephant_shifter_personality
        	    personality = mongoose_shifter_personality
        	    personality = leopard_shifter_personality
        	}
		}
		effect = {
        	add_ruler_personality = leopard_shifter_personality
		}
		ai_will_do = {
			factor = 400
		}
	}
	become_elephant = {
		major = yes
		potential = {
			ai = no
			culture_group = tanizu
		}
		allow = {
			NOT = {
        	    personality = elephant_shifter_personality
        	    personality = mongoose_shifter_personality
        	    personality = leopard_shifter_personality
        	}
		}
		effect = {
        	add_ruler_personality = elephant_shifter_personality
		}
		ai_will_do = {
			factor = 400
		}
	}

	#warlord ai shit
	dbc_war_dec = {
		major = yes
		potential = {
			tag = S30
			ai = yes
			NOT = {
				owns = 5580
			}
		}
		allow = {
			is_year = 1450
			is_at_war = no
			NOT = {
				has_global_flag = dakinshiland_unified
			}
		}
		effect = {
			5580 = {
				owner = {
					ROOT = {
						declare_war_with_cb = {
						    who = PREV
						    casus_belli = cb_annex
						}
					}
				}
			}
		}
		ai_will_do = {
			factor = 400
		}
	}

	#kwizera_first_war = {
	#	major = yes
	#	potential = {
	#		tag = S28
	#		ai = yes
	#	}
	#	allow = {
	#		NOT = {
	#			has_global_flag = dakinshiland_unified
	#		}
	#	}
	#	effect = {
	#		declare_war_with_cb = {
	#		    who = S27
	#		    casus_belli = cb_annex
	#		}
	#	}
	#	ai_will_do = {
	#		factor = 400
	#	}
	#}

	kwizera_akaliza_war = {
		major = yes
		potential = {
			tag = S28
			ai = yes
		}
		allow = {
			S27 = {
				OR = {
					exists = no
					is_subject_of = ROOT
				}
			}
			S29 = { exists = yes }
			NOT = {
				has_global_flag = dakinshiland_unified
			}
			manpower_percentage = 0.65
		}
		effect = {
			declare_war_with_cb = {
			    who = S29
			    casus_belli = cb_annex
			}
		}
		ai_will_do = {
			factor = 400
		}
	}

	akaliza_mwasagore_war = {
		major = yes
		potential = {
			tag = S29
			ai = yes
			is_subject = no
		}
		allow = {
			is_year = 1446
			manpower_percentage = 0.75
			S27 = {
				OR = {
					exists = yes
					NOT = { is_subject_of = ROOT }
				}
			}
			S28 = { exists = no }
			NOT = {
				has_global_flag = dakinshiland_unified
			}
		}
		effect = {
			declare_war_with_cb = {
			    who = S27
			    casus_belli = cb_annex
			}
		}
		ai_will_do = {
			factor = 400
		}
	}
}
