namespace = beast_memory

#hidden events for pulses

province_event = {
	id = beast_memory.1
	title = beast_memory.1.t
	desc = beast_memory.1.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		#religion = beast_memory
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.1.a
        ai_chance = { factor = 100 }
        clr_global_flag = beast_memory_leopard
        clr_global_flag = beast_memory_elephant
        set_global_flag = beast_memory_mongoose
        every_country = {
            #limit = {
            #    religion = beast_memory
            #}
            country_event = { 
                id = beast_memory.4
            }
        }
        set_variable = {
            which = BeastMemoryCounter
            value = 0
        }
        set_variable = { # we use this for loc. It is separate because we might want to mess with it
            which = BeastMemoryYearsLeft
            value = 8
        }
	}
}

province_event = {
	id = beast_memory.2
	title = beast_memory.2.t
	desc = beast_memory.2.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		#religion = beast_memory
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.2.a
        ai_chance = { factor = 100 }
        set_global_flag = beast_memory_leopard
        clr_global_flag = beast_memory_elephant
        clr_global_flag = beast_memory_mongoose
        every_country = {
            #limit = {
            #    religion = beast_memory
            #}
            country_event = { 
                id = beast_memory.5
            }
        }
        set_variable = {
            which = BeastMemoryCounter
            value = 0
        }
        set_variable = { # we use this for loc. It is separate because we might want to mess with it
            which = BeastMemoryYearsLeft
            value = 8
        }
	}
}

province_event = {
	id = beast_memory.3
	title = beast_memory.3.t
	desc = beast_memory.3.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		#religion = beast_memory
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.3.a
        ai_chance = { factor = 100 }
        clr_global_flag = beast_memory_leopard
        clr_global_flag = beast_memory_elephant
        set_global_flag = beast_memory_mongoose
        every_country = {
            #limit = {
            #    religion = beast_memory
            #}
            country_event = { 
                id = beast_memory.6
            }
        }
        set_variable = {
            which = BeastMemoryCounter
            value = 0
        }
        set_variable = { # we use this for loc. It is separate because we might want to mess with it
            which = BeastMemoryYearsLeft
            value = 8
        }
	}
}

country_event = {
	id = beast_memory.4
	title = beast_memory.4.t
	desc = beast_memory.4.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
        #has_global_flag = beast_memory_mongoose
		religion = beast_memory
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
        remove_country_modifier = selfless_elephant_main
        remove_country_modifier = wise_leopard_main
    }

	option = {
		name = beast_memory.4.a
        ai_chance = { factor = 100 }
        trigger = {
            NOT = { has_ruler_modifier = brave_mongoose_ruler }
        }
        add_country_modifier = {
            name = brave_mongoose_main
            duration = -1
        }
	}
    #loc for having a mongoose shifter ruler
    option = {
		name = beast_memory.4.b
        trigger = {
            has_ruler_modifier = brave_mongoose_ruler
        }
        add_country_modifier = {
            name = brave_mongoose_main
            duration = -1
        }
	}
}

country_event = {
	id = beast_memory.5
	title = beast_memory.5.t
	desc = beast_memory.5.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
        #has_global_flag = beast_memory_leopard
		religion = beast_memory
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
        remove_country_modifier = selfless_elephant_main
        remove_country_modifier = brave_mongoose_main
    }

	option = {
		name = beast_memory.5.a
        ai_chance = { factor = 100 }
        trigger = {
            NOT = { has_ruler_modifier = wise_leopard_ruler }
        }
                add_country_modifier = {
            name = wise_leopard_main
            duration = -1
        }
	}
    #loc for having a leopard shifter ruler
    option = {
		name = beast_memory.5.c
        ai_chance = { factor = 100 }
        trigger = {
            has_ruler_modifier = wise_leopard_ruler
        }
        add_country_modifier = {
            name = wise_leopard_main
            duration = -1
        }
	}
}

country_event = {
	id = beast_memory.6
	title = beast_memory.6.t
	desc = beast_memory.6.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
        #has_global_flag = beast_memory_elephant
		religion = beast_memory
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
        remove_country_modifier = wise_leopard_main
        remove_country_modifier = brave_mongoose_main
    }

	option = {
		name = beast_memory.6.a
        ai_chance = { factor = 100 }
        trigger = {
            NOT = { has_ruler_modifier = selfless_elephant_ruler }
        }
        add_country_modifier = {
            name = selfless_elephant_main
            duration = -1
        }
	}
    #loc for having a mongoose shifter ruler
    option = {
		name = beast_memory.6.c
        ai_chance = { factor = 100 }
        trigger = {
            has_ruler_modifier = selfless_elephant_ruler
        }
        add_country_modifier = {
            name = selfless_elephant_main
            duration = -1
        }
	}
}

#province event to handle the recursion
province_event = {
	id = beast_memory.7
	title = beast_memory.7.t
	desc = beast_memory.7.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		#NOT = {
        #    has_province_flag = beast_memory_stop
        #}
        always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.7.a
        ai_chance = { factor = 100 }
        trigger = {
            NOT = {
                has_province_flag = beast_memory_stop
            }
        }
        S28 = {
            add_prestige = 4
        }
        if = { #if mongoose and eight years have passed, go to leopard
            limit = {
                has_global_flag = beast_memory_mongoose
                check_variable = {
                    which = BeastMemoryCounter
                    value = 7
                }
            }
            province_event = { 
                id = beast_memory.2
            }
            #set_variable = { #happens elsewhere
            #    which = BeastMemoryCounter
            #    value = 0
            #}
        }
        else_if = { #go to elephant
            limit = {
                has_global_flag = beast_memory_leopard
                check_variable = {
                    which = BeastMemoryCounter
                    value = 7
                }
            }
            province_event = { 
                id = beast_memory.3
            }
            #set_variable = {
            #    which = BeastMemoryCounter
            #    value = 0
            #}
        }
        else_if = { #go to mongoose
            limit = {
                has_global_flag = beast_memory_elephant
                check_variable = {
                    which = BeastMemoryCounter
                    value = 7
                }
            }
            province_event = { 
                id = beast_memory.1
            }
            #set_variable = {
            #    which = BeastMemoryCounter
            #    value = 0
            #}
        }
        else = { #if we don't change we increment
            province_event = {
                id = beast_memory.8
            }
        }
	}
}

province_event = {
	id = beast_memory.8
	title = beast_memory.8.t
	desc = beast_memory.8.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.2.a
        ai_chance = { factor = 100 }
        change_variable = {
            which = BeastMemoryCounter
            value = 1
        }
        change_variable = { # we use this for loc. It is separate because we might want to mess with it
            which = BeastMemoryYearsLeft
            value = -1
        }
	}
}

province_event = {
	id = beast_memory.9
	title = beast_memory.9.t
	desc = beast_memory.9.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.2.a
        ai_chance = { factor = 100 }
	}
}

country_event = {
	id = beast_memory.10
	title = beast_memory.10.t
	desc = beast_memory.10.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes
    fire_only_once = yes
	hidden = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
        1794 = {
            set_variable = {
                which = BeastMemoryCounter
                value = 0 #we increment this later, so it starts at 1 in 1444
            }
            set_variable = { # we use this for loc. It is separate because we might want to mess with it
                which = BeastMemoryYearsLeft
                value = 8 #we decrement this later
            }
        }
        clr_global_flag = beast_memory_setup_flag
        clr_global_flag = beast_memory_mongoose
        set_global_flag = beast_memory_setup_flag
        set_global_flag = beast_memory_mongoose
        1794 = {
	    	province_event = { 
                id = beast_memory.7
            } #spawns an event in the salahad to do beast memory calculations
	    }
        REB = {
            set_country_flag = check_flags
            set_global_flag = check_g
        }
    }

	option = {
		name = beast_memory.2.a
	}
}


#religious actions

country_event = {
	id = beast_memory.11
	title = beast_memory.11.t
	desc = beast_memory.11.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.11.a
        ai_chance = { factor = 100 }
        custom_tooltip = "Random Administrative Advisor"
        hidden_effect = {
            random_list = {
                10 = {
                    define_advisor = {
                        type = philosopher
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = natural_scientist
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = artist
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = treasurer
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = theologian
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = master_of_mint
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = inquisitor
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = court_mage
                        skill = 1
                        discount = yes
                    }
                }
            }
        }
	}
    option = {
		name = beast_memory.11.b
        ai_chance = { factor = 100 }
        custom_tooltip = "Random Diplomatic Advisor, excluding Naval and Colonial advisors"
        hidden_effect = {
            random_list = {
                10 = {
                    define_advisor = {
                        type = statesman
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = trader
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = spymaster
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = diplomat
                        skill = 1
                        discount = yes
                    }
                }
            }
        }
	}
	option = {
		name = beast_memory.11.c
        ai_chance = { factor = 100 }
        custom_tooltip = "Random Military Advisor"
        hidden_effect = {
            random_list = {
                10 = {
                    define_advisor = {
                        type = army_reformer
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = army_organiser
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = commandant
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = quartermaster
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = recruitmaster
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = fortification_expert
                        skill = 1
                        discount = yes
                    }
                }
                10 = {
                    define_advisor = {
                        type = grand_captain
                        skill = 1
                        discount = yes
                    }
                }
            }
        }
	}
}

country_event = {
	id = beast_memory.12
	title = beast_memory.12.t
	desc = beast_memory.12.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		always = yes
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.12.a
        ai_chance = { factor = 100 }
        random_core_province = {
            limit = {
                owned_by = ROOT
            }
            random_list = {
                10 = {
                    add_base_tax = 1
                }
                10 = {
                    add_base_production = 1
                }
                10 = {
                    add_base_manpower = 1
                }
            }
        }
	}
}

#make ruler a shifter
country_event = {
	id = beast_memory.13
	title = beast_memory.13.t
	desc = beast_memory.13.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		NOT = {
            personality = elephant_shifter_personality
            personality = mongoose_shifter_personality
            personality = leopard_shifter_personality
        }
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.13.a
        ai_chance = { factor = 100 }
        add_ruler_personality = mongoose_shifter_personality
	}
}

country_event = {
	id = beast_memory.14
	title = beast_memory.14.t
	desc = beast_memory.14.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		NOT = {
            personality = elephant_shifter_personality
            personality = mongoose_shifter_personality
            personality = leopard_shifter_personality
        }
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.14.a
        ai_chance = { factor = 100 }
        add_ruler_personality = leopard_shifter_personality
	}
}

country_event = {
	id = beast_memory.15
	title = beast_memory.15.t
	desc = beast_memory.15.d
	picture = BURGHER_ESTATE_eventPicture

	is_triggered_only = yes

	trigger = {
		NOT = {
            personality = elephant_shifter_personality
            personality = mongoose_shifter_personality
            personality = leopard_shifter_personality
        }
	}

	mean_time_to_happen = {
		days = 1
	}

    immediate = {
    }

	option = {
		name = beast_memory.15.a
        ai_chance = { factor = 100 }
        add_ruler_personality = elephant_shifter_personality
	}
}
